<section class="w-100 hero-top-primary position-absolute">
    <div class="container-fluid">
        <div class="row">
            <nav class="navbar py-2 col-12" id="cordaempresa">
                <a class="navbar-brand" href="#">Logo da empresa</a>
                <div class="btn-group ">
                    <a href="#" class="mr-3 text-white "><i
                                class="fas fa-home "></i></a>
                    <a href="#" data-toggle="modal" data-target="#sobre"
                       class="mr-3 ml-2 links-navbar text-decoration-none">SOBRE A
                        CAMPANHA</a>
                    <a href="#" data-toggle="modal" data-target="#ranking"
                       class="ml-2 links-navbar text-decoration-none">RANKING</a>
                    <div>
            </nav>
            <!-- Modal -->
            <div class="modal fade mt-5" id="sobre" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div class="modal-dialog modal-xl mt-4" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="row d-flex justify-content-center">
                                <img src="###" width="70" height="70"
                                     alt="">imagem
                            </div>
                            <div class="row mt-3 py-3 mx-1 d-flex justify-content-center bg-secondary">
                                <h3 class="font-weight-bold">SOBRE A CAMPANHA</h3>
                            </div>
                            <div class="row d-flex justify-content-center py-3">
                                <h6>Esse no caso é o titulo da campanha</h6>
                            </div>
                            <div class="row">
                                <p class="px-3">
                                    Contrary to popular belief, Lorem Ipsum is not simply
                                    random text.
                                    It has roots in a piece of classical Latin literature
                                    from 45 BC
                                    making it over 2000 years old. Richard McClintock, a
                                    Latin
                                    professor at Hampden-Sydney College in Virginia, looked
                                    up one of
                                    the more obscure Latin words, consectetur, from a Lorem
                                    Ipsum
                                    passage, and going through the cites of the word in
                                    classical
                                    literature, discovered the undoubtable source. Lorem
                                    Ipsum
                                </p>
                                <p class="px-3">
                                    Contrary to popular belief, Lorem Ipsum is not simply
                                    random text.
                                    It has roots in a piece of classical Latin literature
                                    from 45 BC
                                    making it over 2000 years old. Richard McClintock, a
                                    Latin
                                    professor at Hampden-Sydney College in Virginia, looked
                                    up one of
                                    the more obscure Latin words, consectetur, from a Lorem
                                    Ipsum
                                    passage, and going through the cites of the word in
                                    classical
                                    literature, discovered the undoubtable source. Lorem
                                    Ipsum
                                </p>
                                <p class="px-3">
                                    Contrary to popular belief, Lorem Ipsum is not simply
                                    random text.
                                    It has roots in a piece of classical Latin literature
                                    from 45 BC
                                    making it over 2000 years old. Richard McClintock, a
                                    Latin
                                    professor at Hampden-Sydney College in Virginia, looked
                                    up one of
                                    the more obscure Latin words, consectetur, from a Lorem
                                    Ipsum
                                    passage, and going through the cites of the word in
                                    classical
                                    literature, discovered the undoubtable source. Lorem
                                    Ipsum
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade escurecer-fundo-modal pt-4 px-5" id="ranking"
                 tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
                 aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <div class="row d-flex justify-content-center pt-2 pb-3 escurecer-fundo-modal">
                            <h3 class="text-white font-weight-bold">RANKING GERAL</h3>
                        </div>
                        <div class="row d-flex justify-content-center pb-5 escurecer-fundo-modal">
                            <h6 class="text-white">Este relatório compõe as 30 fotos mais
                                curtidas da empresa VIVAWEB INTERNET</h6>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <img src="./imagens/mae.jpg" width="1200px" height="700px"
                                     alt="">
                            </div>
                            <div class="row">
                                <table class="table table-striped">
                                    <thead class="thead-light">
                                    <tr>
                                        <th scope="col">POSIÇÃO</th>
                                        <th scope="col">COLABORADOR</th>
                                        <th scope="col">DAPARTAMENTO</th>
                                        <th scope="col">CURTIDAS</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row"><span class="mr-5 ml-2">1.</span><i
                                                    class="fas fa-medal fa-2x"></i></th>
                                        <td><i class="fas fa-user-circle fa-2x"></i><span
                                                    class="ml-3 pb-2">NOME DO COLABORADOR</span>
                                        </td>
                                        <td class="pl-4">MARKETING</td>
                                        <td class="pl-5">53</td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><span class="mr-5 ml-2">2.</span><i
                                                    class="fas fa-medal fa-2x"></i></th>
                                        <td><i class="fas fa-user-circle fa-2x"></i><span
                                                    class="ml-3 pb-2">NOME DO COLABORADOR</span>
                                        </td>
                                        <td class="pl-4">MARKETING</td>
                                        <td class="pl-5">53</td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><span class="mr-5 ml-2">3.</span><i
                                                    class="fas fa-medal fa-2x"></i></th>
                                        <td><i class="fas fa-user-circle fa-2x"></i><span
                                                    class="ml-3 pb-2">NOME DO COLABORADOR</span>
                                        </td>
                                        <td class="pl-4">MARKETING</td>
                                        <td class="pl-5">53</td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><span class="ml-2">4.</th>
                                        <td><i class="fas fa-user-circle fa-2x"></i><span
                                                    class="ml-3 pb-2">NOME DO COLABORADOR</span>
                                        </td>
                                        <td class="pl-4">MARKETING</td>
                                        <td class="pl-5">53</td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><span class="ml-2">5.</th>
                                        <td><i class="fas fa-user-circle fa-2x"></i><span
                                                    class="ml-3 pb-2">NOME DO COLABORADOR</span>
                                        </td>
                                        <td class="pl-4">MARKETING</td>
                                        <td class="pl-5">53</td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><span class="ml-2">6.</th>
                                        <td><i class="fas fa-user-circle fa-2x"></i><span
                                                    class="ml-3 pb-2">NOME DO COLABORADOR</span>
                                        </td>
                                        <td class="pl-4">MARKETING</td>
                                        <td class="pl-5">53</td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><span class="ml-2">7.</th>
                                        <td><i class="fas fa-user-circle fa-2x"></i><span
                                                    class="ml-3 pb-2">NOME DO COLABORADOR</span>
                                        </td>
                                        <td class="pl-4">MARKETING</td>
                                        <td class="pl-5">53</td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><span class="ml-2">8.</th>
                                        <td><i class="fas fa-user-circle fa-2x"></i><span
                                                    class="ml-3 pb-2">NOME DO COLABORADOR</span>
                                        </td>
                                        <td class="pl-4">MARKETING</td>
                                        <td class="pl-5">53</td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><span class="ml-2">9.</th>
                                        <td><i class="fas fa-user-circle fa-2x"></i><span
                                                    class="ml-3 pb-2">NOME DO COLABORADOR</span>
                                        </td>
                                        <td class="pl-4">MARKETING</td>
                                        <td class="pl-5">53</td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><span class="ml-2">10.</th>
                                        <td><i class="fas fa-user-circle fa-2x"></i><span
                                                    class="ml-3 pb-2">NOME DO COLABORADOR</span>
                                        </td>
                                        <td class="pl-4">MARKETING</td>
                                        <td class="pl-5">53</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="modal fade background-color-modal" id="exempleModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="min-width: 952px">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="col-lg-12 mb-3">
                        <div class="row">
                            <i class="far fa-image m-auto"></i>
                        </div>
                    </div>
                    <div class="col-lg-12 bg-secondary mb-3">
                        <div class="row">
                            <h3 class="m-auto">
                                Sobre a Campanha
                            </h3>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="row">
                            <h5 class="m-auto">
                                O que é Lorem Ipsum?
                            </h5>
                            <p class="mt-2">
                                Lorem Ipsum é simplesmente uma simulação de texto da indústria
                                tipográfica e de impressos, e vem sendo utilizado desde o século
                                XVI, quando um impressor desconhecido pegou uma bandeja de tipos
                                e os embaralhou para fazer um livro de modelos de tipos. Lorem
                                Ipsum sobreviveu não só a cinco séculos, como também ao salto
                                para a editoração eletrônica, permanecendo essencialmente
                                inalterado.
                            </p>
                            <p class="mt-2">
                                Lorem Ipsum é simplesmente uma simulação de texto da indústria
                                tipográfica e de impressos, e vem sendo utilizado desde o século
                                XVI, quando um impressor desconhecido pegou uma bandeja de tipos
                                e os embaralhou para fazer um livro de modelos de tipos. Lorem
                                Ipsum sobreviveu não só a cinco séculos, como também ao salto
                                para a editoração eletrônica, permanecendo essencialmente
                                inalterado.
                            </p>
                            <p class="mt-2">
                                Lorem Ipsum é simplesmente uma simulação de texto da indústria
                                tipográfica e de impressos, e vem sendo utilizado desde o século
                                XVI, quando um impressor desconhecido pegou uma bandeja de tipos
                                e os embaralhou para fazer um livro de modelos de tipos. Lorem
                                Ipsum sobreviveu não só a cinco séculos, como também ao salto
                                para a editoração eletrônica, permanecendo essencialmente
                                inalterado.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade background-color-modal" id="exempleModalTwo" tabindex="-1"
         role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="min-width: 952px">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="col-lg-12 mb-3">
                        <div class="row">
                            <i class="far fa-image m-auto"></i>
                        </div>
                    </div>
                    <div class="col-lg-12 bg-secondary mb-3">
                        <div class="row">
                            <h3 class="m-auto">
                                Sobre a Campanha
                            </h3>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="row">
                            <h5 class="m-auto">
                                O que é Lorem Ipsum?
                            </h5>
                            <p class="mt-2">
                                Lorem Ipsum é simplesmente uma simulação de texto da indústria
                                tipográfica e de impressos, e vem sendo utilizado desde o século
                                XVI, quando um impressor desconhecido pegou uma bandeja de tipos
                                e os embaralhou para fazer um livro de modelos de tipos. Lorem
                                Ipsum sobreviveu não só a cinco séculos, como também ao salto
                                para a editoração eletrônica, permanecendo essencialmente
                                inalterado.
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="w-100 position-absolute hero-top">
    <div class="container">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <h2 class="hero-text-h2 m-auto">Photography Is An Immediate
                            Reaction,
                            <span class="d-flex justify-content-center">Drawing Is A Meditation Props.</span></h2>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <p class="hero-text-p">Photography is a way of feeling, of touching, of loving. What you have
                            caught on film is
                            captured forever... it <span class="d-flex justify-content-center">remembers little things, long after you have forgotten
                            everything.</span></p>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="m-auto">
                            <div class="btn hero-btn">
                                <p class="mt-2">CONTACT US</p>
                            </div>
                            <div class="btn hero-btn-secondary ml-3">
                                <p class="mt-2">PORTIFOLIO</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="m-auto">
                            <div class="mt-5">
                                <a href="#hero-photo">
                                    <i class="fa fa-angle-double-down hero-icon" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>