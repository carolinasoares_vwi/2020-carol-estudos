<section>
    <div class="bgbar"></div>
    <footer class="footer-section">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 my-5">
                        <img src="https://martinineto.com.br/wp-content/themes/martiniv2/css/img/logorodape.png" alt="">
                    </div>
                    <div class="col-lg-6 my-5">
                        <div id="social">
                            <ul class="socialList">
                                <li>
                                    <a href="https://www.facebook.com/martini.neto.12" target="_blank"><i
                                                class="fab fa-facebook-f"></i></a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/martinineto" target="_blank"><i
                                                class="fab fa-instagram"></i></a>
                                </li>
                                <li>
                                    <a href="https://www.linkedin.com/in/martinineto" target="_blank"><i
                                                class="fab fa-linkedin"></i></a>
                                </li>
                                <li>
                                    <a href="https://www.youtube.com/channel/UCzwNA2suEG1bFE-22Hp-DHA/featured"
                                       target="_blank"><i class="fab fa-youtube"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 my-5">
                        <ul class="menuList">
                            <li><a href="https://martinineto.com.br/?cat=1">Blog</a></li>
                            <li><a href="/?page_id=22">Cursos</a></li>
                            <li><a href="http://www.multibot.com.br" target="_blank">Multibot</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</section>
